#!/usr/bin/env python3
from mininet.topo import Topo
from mininet import net
from mininet.net import Mininet


class MyTopo(Topo):
    """
    A Simple Topo
    """

    def __init__(self):
        # Initialize topology
        Topo.__init__(self)

        self.addHost('h1', mac='00:00:00:00:00:01')
        self.addHost('h2', mac='00:00:00:00:00:02')
        self.addHost('h3', mac='00:00:00:00:00:03')
        self.addHost('h4', mac='00:00:00:00:00:04')

        self.addSwitch('s1')
        self.addSwitch('s2')
        self.addSwitch('s3')

        self.addLink('s1', 's2')
        self.addLink('s1', 's3')
        self.addLink('s2', 'h1')
        self.addLink('s2', 'h2')
        self.addLink('s3', 'h3')
        self.addLink('s3', 'h4')


topos = {'mytopo': (lambda: MyTopo())}
