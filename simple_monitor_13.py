# Copyright (C) 2016 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from operator import attrgetter

# from ryu.app import simple_switch_13
from simple_switch_13 import SimpleSwitch13

from ryu.controller import ofp_event
from ryu.controller.handler import MAIN_DISPATCHER, DEAD_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.lib import hub


# class SimpleMonitor13(simple_switch_13.SimpleSwitch13):
class SimpleMonitor13(SimpleSwitch13):

    def __init__(self, *args, **kwargs):
        super(SimpleMonitor13, self).__init__(*args, **kwargs)
        self.datapaths = {}
        self.leaved_count = {}
        self.arrived_count = {}
        self.monitor_thread = hub.spawn(self._monitor)

    @set_ev_cls(ofp_event.EventOFPStateChange,
                [MAIN_DISPATCHER, DEAD_DISPATCHER])
    def _state_change_handler(self, ev):
        datapath = ev.datapath
        if ev.state == MAIN_DISPATCHER:
            if datapath.id not in self.datapaths:
                self.logger.debug('register datapath: %016x', datapath.id)
                self.datapaths[datapath.id] = datapath
        elif ev.state == DEAD_DISPATCHER:
            if datapath.id in self.datapaths:
                self.logger.debug('unregister datapath: %016x', datapath.id)
                del self.datapaths[datapath.id]

    def _monitor(self):
        self.leave_list = []
        self.arrive_list = []
        self.leave_tpc = 0
        self.leave_tbc = 0
        self.arrive_tpc = 0
        self.arrive_tbc = 0
        while True:
            for dp in self.datapaths.values():
                self._request_stats(dp)
            hub.sleep(10)
            # leave
            self.logger.info('\n\n<< ========== leaved host1 ========== >>')
            self.logger.info('        datapath '
                             ' in-port           eth-src '
                             'out-port  packets    bytes')
            self.logger.info('---------------- '
                             '-------- ----------------- '
                             '-------- -------- --------')
            for i in self.leave_list:
                self.logger.info(i)
            self.logger.info('total packet count: %8d, total byte count: %8d' % (self.leave_tpc, self.leave_tbc))
            # arrive
            self.logger.info('<< ========== arrived host1 ========== >>')
            self.logger.info('        datapath '
                             ' in-port           eth-dst '
                             'out-port  packets    bytes')
            self.logger.info('---------------- '
                             '-------- ----------------- '
                             '-------- -------- --------')
            for i in self.arrive_list:
                self.logger.info(i)
            self.logger.info('total packet count: %8d, total byte count: %8d' % (self.arrive_tpc, self.arrive_tbc))
            # clear
            self.leave_list.clear()
            self.arrive_list.clear()
            self.leave_tpc = 0
            self.leave_tbc = 0
            self.arrive_tpc = 0
            self.arrive_tbc = 0

    def _request_stats(self, datapath):
        self.logger.debug('send stats request: %016x', datapath.id)
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        req = parser.OFPFlowStatsRequest(datapath)
        datapath.send_msg(req)

        req = parser.OFPPortStatsRequest(datapath, 0, ofproto.OFPP_ANY)
        datapath.send_msg(req)

    @set_ev_cls(ofp_event.EventOFPFlowStatsReply, MAIN_DISPATCHER)
    def _flow_stats_reply_handler(self, ev):
        body = ev.msg.body
        for stat in sorted([flow for flow in body if flow.priority == 1 and flow.instructions != []],
                           key=lambda flow: (flow.match['in_port'],
                                             flow.match['eth_src'])):
            if stat.match['eth_src'] == '00:00:00:00:00:01':
                in_port = stat.match['in_port']
                out_port = stat.instructions[0].actions[0].port
                dpid = ev.msg.datapath.id
                if not self.leaved_count.get((dpid, in_port, out_port)):
                    self.leaved_count[(dpid, in_port, out_port)] = (0, 0)
                pkg_count = stat.packet_count - self.leaved_count[(dpid, in_port, out_port)][0]
                pkg_byte = stat.byte_count - self.leaved_count[(dpid, in_port, out_port)][1]
                if pkg_count > 0:
                    self.leave_list.append('%16x %8x %17s %8x %8d %8d' % (dpid,
                                                                      in_port, stat.match['eth_src'],
                                                                      out_port,
                                                                      pkg_count, pkg_byte))
                self.leave_tpc = self.leave_tpc + pkg_count
                self.leave_tbc = self.leave_tbc + pkg_byte
                self.leaved_count[(dpid, in_port, out_port)] = (stat.packet_count, stat.byte_count)
        for stat in sorted([flow for flow in body if flow.priority == 1 and flow.instructions != []],
                           key=lambda flow: (flow.match['in_port'],
                                             flow.match['eth_dst'])):
            if stat.match['eth_dst'] == '00:00:00:00:00:01':
                in_port = stat.match['in_port']
                out_port = stat.instructions[0].actions[0].port
                dpid = ev.msg.datapath.id
                if not self.arrived_count.get((dpid, in_port, out_port)):
                    self.arrived_count[(dpid, in_port, out_port)] = (0, 0)
                pkg_count = stat.packet_count - self.arrived_count[(dpid, in_port, out_port)][0]
                pkg_byte = stat.byte_count - self.arrived_count[(dpid, in_port, out_port)][1]
                if pkg_count > 0:
                    self.arrive_list.append('%16x %8x %17s %8x %8d %8d' % (dpid,
                                                                      in_port, stat.match['eth_dst'],
                                                                      out_port,
                                                                      pkg_count, pkg_byte))
                self.arrive_tpc = self.arrive_tpc + pkg_count
                self.arrive_tbc = self.arrive_tbc + pkg_byte
                self.arrived_count[(dpid, in_port, out_port)] = (stat.packet_count, stat.byte_count)
