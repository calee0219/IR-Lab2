#!/usr/bin/env python

from mytopo import MyTopo

import time
from mininet.net import Mininet
from mininet.link import TCLink
from mininet.util import dumpNodeConnections
from mininet.log import setLogLevel
from mininet.node import RemoteController, OVSSwitch
from mininet.cli import CLI
from functools import partial


def test_net():
    topo = MyTopo()
    switch = partial(OVSSwitch, protocols='OpenFlow13')
    net = Mininet(topo=topo, link=TCLink, controller=None)
    net.addController('myController', controller=RemoteController, \
                      ip='127.0.0.1', port=6653)

    # net.waitConnected()
    # Start net testing
    net.start()
    h2 = net.get('h2')
    h3 = net.get('h3')
    net.pingAll(timeout=1)
    net.stop()

if __name__ == '__main__':
    setLogLevel('info')
    test_net()
